from numpy import *
import numpy as np
import pdb

def computeMixtureFraction(MFtype,Yfuel,Yoxy,Yfuel_bot,Yoxy_top,Yco2,Yh2o):
  mO2=15.99*2.0
  vO2=11.0
  mC7H16=100.21
  vC7H16=1.0
  v=vO2*mO2/(vC7H16*mC7H16)
  if MFtype=="fuelair":
    Z=(v*Yfuel-Yoxy+Yoxy_top)/(v*Yfuel_bot+Yoxy_top)
  elif MFtype=="bilger":
    m=7.0
    n=16.0
    mC=12.0107
    mH=1.00794
    mO=15.99
    mO2=mO*2
    mCO2=43.9907
    mH2O=18.00588
    Y_O2_init=0.2
    Y_F_init=0.95
    Z_C=7.0*(mC/mC7H16)*Yfuel+mC/mCO2*Yco2
    Z_C1=7.0*(mC/mC7H16)*Y_F_init
    Z_H=16.*mH/mC7H16*Yfuel + 2*(mH/mH2O)*Yh2o
    Z_H1=16.*mH/mC7H16*Y_F_init
    Z_O=2*mO/mO2*Yoxy + 2*mO/mCO2*Yco2+mH/mH2O*Yh2o
    Z= (Z_C/(m*mC) + Z_H/(n*mH)+ 2.0*(Y_O2_init-Z_O)/(v*mO2))/(Z_C1/(m*mC) + Z_H1/(n*mH) +2.0*Y_O2_init/(v*mO2))
  #clip non-physical values
  Z[Z>1.0] = 1.0
  Z[Z<0] = 0
  return Z
