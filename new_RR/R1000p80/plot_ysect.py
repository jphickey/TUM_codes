import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from scipy import interpolate
from matplotlib import rc
import pickle
rc('text', usetex=True)
font = {'family' : 'sans-serif','sans-serif':['Helvetica'],'weight' : 'bold','size'   : 14}
rc('font', **font)
DNS = pickle.load( open( "DNS.p", "rb" ) )
CSE = pickle.load( open( "CSE.p", "rb" ) )
DCSE = pickle.load( open( "DCSE.p", "rb" ) )
PDFDCSE = pickle.load( open( "PDFDCSE.p", "rb" ) )
PDFCSE = pickle.load( open( "PDFCSE.p", "rb" ) )
omega_wrng = pickle.load( open( "omega_wrng.p", "rb" ) )
X2 = pickle.load( open( "x2_vort.p", "rb" ) )
DNS_filtered = pickle.load( open( "DNS_filtered.p", "rb" ) )
CSE_filtered = pickle.load( open( "CSE_filtered.p", "rb" ) )
DCSE_filtered = pickle.load( open( "DCSE_filtered.p", "rb" ) )
y_mid = pickle.load( open( "y_mid.p", "rb" ) )
for i in range(len(y_mid)):
  y_mid[i] = X2[int(y_mid[i])]

#plt.plot(X2,omega_wrng, label=r'$\dot{\omega}(\overline{T},\overline{\rho},\overline{Y_{\alpha}})$')
plt.plot(X2,DNS,color='r',linewidth=2.0,label = '$\overline{\dot{\omega}}$')
#plt.plot(y_mid,DNS_filtered,label = 'DNS, large ensemble')
plt.plot(X2,CSE,color='m', label='$\overline{\dot{\omega}}_{CSE}$, DNS-PDF')
#plt.plot(y_mid,CSE_filtered, label='CSE, DNS-PDF, large ensemble')
#plt.plot(X2,PDFCSE,color='m', label='$\overline{\dot{\omega}}_{CSE}$, Beta-PDF')
plt.plot(X2,DCSE,color='b', label=r'$\overline{\dot{\omega}}_{DCSE}$, DNS-PDFs')
#plt.plot(y_mid,DCSE_filtered, label=r'DCSE, DNS-PDFs, large ensemble')
#plt.plot(X2,PDFDCSE, label=r'DCSE, Beta-PDF for $\zeta$, Norm-PDF for $T\vert \zeta$')

    #plt.plot(PDFDCSE, label='DCSE and Gamma-PDF')
    #plt.plot(PDFCSE, label='DCSE and Gamma-PDF for mixture')
PDFDCSE = np.nan_to_num(PDFDCSE)
PDFCSE = np.nan_to_num(PDFCSE)
normerr = np.sum(np.power((DNS-CSE),2))/len(CSE)
print "CSE" ,np.sum(np.power((DNS-CSE),2))/len(CSE)
print "DCSE" ,np.sum(np.power((DNS-DCSE),2))/(normerr*len(CSE))
print "PDFDCSE" ,np.sum(np.power((DNS-PDFDCSE),2))/(normerr*len(CSE))
print "PDFCSE" ,np.sum(np.power((DNS-PDFCSE),2))/(normerr*len(CSE))

plt.xlabel(r'$x_2/\delta_{\omega,0}$')
plt.ylabel(r'reaction rate  mol (m$^3$ s)$^{-1}$ ')
plt.ylim(0,0.11)
#plt.xlim(0,42000)
plt.legend()
plt.tight_layout()
plt.savefig("Re1000p60a_DCSE.pdf")
plt.show()


