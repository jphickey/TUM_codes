import numpy as np
import matplotlib.pyplot as plt
import pdb
#Destribution as per the paper
Aexp=9.6E6  # cm^3/(mol x s)
a=1
b=1
rho_max=241.0
m_hept=100.21  # g/mol
m_o2  = 32.0   # g/mol
Y_hept=Y_o2=0.5
Eact=30    #kcal/mol
Ru= 1.9872036E-3 # kcal/(K mol)
T_max = 2500      #K
T=np.linspace(0,T_max)
#JPH based on Josette's paper
omega= Aexp * rho_max**(a+b)/(m_hept*m_o2)*Y_hept*Y_o2*np.exp(-Eact/(Ru*T))
plt.plot(T,omega,label="josette")

# Previous paper
Aexp=5.1E11  # cm^3/(mol x s)
omega= Aexp * rho_max**(a+b)/(m_hept*m_o2)*Y_hept*Y_o2*np.exp(-Eact/(Ru*T))
plt.plot(T,omega,label="old")
plt.legend()
plt.show()

#Josette's email
T=2586.0
Aexp =5.11E11
Aexp * rho_max**(a+b)/(m_hept*m_o2)*Y_hept*Y_o2*np.exp(-Eact/(Ru*T))
pdb.set_trace()

6.745E9
