from numpy import *
import pdb
import scipy
import numpy as np
from scipy import stats
##
#
##
#inputs: Temperature,Pressure,mass fractions,conditioning variable
def calc_omega(T,rho,Y_1,Y_2):
  #calculate omega

  A=9.6e6#cm^3(mol x s)^-1
  Ea=30.0#kcal/mol
  b=1
  a_sm=1
  n=0
  Ru=1.98588E-3 #kcal (K mol)-1
  m_1 = 100  ###C7H16  g/mol
  m_2 = 32 ## O2 g/mol

  return A*np.power(rho,a_sm+b)/(np.power(m_1,a_sm)*np.power(m_2,b))*np.power(Y_1,a_sm)*np.power(Y_2,b)*np.exp(-Ea/(Ru*T))

#perfect match
#Ru=8.31314/4.414
#A=2.75E9




def molar_mass(Y_1,Y_2,Y_3,Y_4):
  m_1 = 100.21   ## C7H16  g/mol
  m_2 = 31.9988  ## O2     g/mol
  m_3 = 44.01    ## CO2    g/mol
  m_4 = 18.01528 ## H2O    g/mol
  m_5 = 28.0134  ## N2     g/mol

  Y_5 = 1 - Y_1 - Y_2 - Y_3 - Y_4

  M_tot = 1/(Y_1/m_1 + Y_2/m_2 + Y_3/m_3 + Y_4/m_4 + Y_5/m_5)

  return M_tot
