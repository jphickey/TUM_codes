import numpy as np
import pdb

#tool.compute_Zcomp(tmp,prs,rho,mixture_mass)
def compute_Zcomp(T,P,rho,M):
  Ru=8314.4598
  return P/(rho*T*Ru/M)


def blockLoc(myid,order):
  return np.where(order==myid)


def globalCoord(myid,order,nx,ny,nz):
    bi,bj,bk=blockLoc(myid,order)
    xglob = range(nx*bi,nx*(bi+1))
    yglob = range(ny*bj,ny*(bj+1))
    zglob = range(nz*bk,nz*(bk+1))
    return xglob,yglob,zglob,bi,bj,bk

def avg_sqr_y(val):
  temp = np.square(val)
  return temp.mean(axis=0).mean(axis=1)

# compute the average in the y direction
def avg_y(val):
  return val.mean(axis=0).mean(axis=1)

# compute the average over all directions
def avg(val):
  return  val.mean()

# compute the max over all directions
def max(val):
  return  np.max(val)

# compute the maximum in the y direction
def max_y(val):
  return  val.max(axis=0).max(axis=1)

def tstar(tmp,z):
  T = tmp
  Tstar = np.zeros((len(tmp),len(tmp[0,:,0]),len(tmp[0,0,:])))
  Tox = 1000
  Tf  = 600
  Tst = 3000
  zst = 0.075
  #Tstar = (T-Tox)*zst*zst/(2*z) + (T-Tst)*(1-zst)**2/(2*z-2*zst)
  Tstar = np.where(z>zst,T - (z-zst)/(1-zst)*(Tf-Tst)-Tst,T + (z-zst)/(zst)*(Tox-Tst)-Tst)
  #Tstar[z<=zst] = T + (z-zst)/(zst)*(Tox-Tst)-Tst
  return Tstar

def quantify_err(var,var_filtered):
  return np.sum(np.power((var-var_filtered),2))/np.count(var)

def filterData_cell(tmp,myid,cell):
    nx,ny,nz=np.shape(tmp)

def createCoarse(mx,my,mz,cell_to_avg):
    max_x=int(np.floor(mx/cell_to_avg))
    max_y=int(np.floor(my/cell_to_avg))
    max_z=int(np.floor(mz/cell_to_avg))
    '''if mx%cell_to_avg !=0:
        max_x = (int(np.floor(mx/cell_to_avg))-1)
    if  my%cell_to_avg !=0:
        max_y = (int(np.floor(my/cell_to_avg))-1)
    if  mz%cell_to_avg !=0:
        max_z = (int(np.floor(mz/cell_to_avg))-1)'''
    return np.zeros([max_x,max_y,max_z])

def filterData_cell(phi,shape,nx_index,ny_index,nz_index):
   coarse=np.zeros(shape)
   coarse_x=np.array(range(min(nx_index),max(nx_index)+1))
   coarse_y=np.array(range(min(ny_index),max(ny_index)+1))
   coarse_z=np.array(range(min(nz_index),max(nz_index)+1))
   avg_cell_x=[None] * np.size(coarse_x)
   avg_cell_y=[None] * np.size(coarse_y)
   avg_cell_z=[None] * np.size(coarse_z)
   #pdb.set_trace()
   for ii,blockNb in enumerate(coarse_x):
      avg_cell_x[ii]=np.where(nx_index==blockNb)[0]
   for jj,blockNb in enumerate(coarse_y):
       avg_cell_y[jj]=np.where(ny_index==blockNb)[0]
   for kk,blockNb in enumerate(coarse_z):
       avg_cell_z[kk]=np.where(nz_index==blockNb)[0]
   for kk,bk in enumerate(coarse_z):
       for jj,bj in enumerate(coarse_y):
           for ii,bi in enumerate(coarse_x):
             coarse[bi,bj,bk]=np.sum(phi[avg_cell_x[ii][0]:avg_cell_x[ii][-1]+1,avg_cell_y[jj][0]:avg_cell_y[jj][-1]+1,avg_cell_z[kk][0]:avg_cell_z[kk][-1]+1])
   return coarse

def getFilter(cell_to_avg, xglob,yglob,zglob,mx,my,mz):
    max_x=max_y=max_z=int(1E9)
    #if xglob[-1]==mx-1 :
    max_x = (int(np.floor((mx-1)/cell_to_avg)))
    #if yglob[-1]==my-1 :
    max_y = (int(np.floor((my-1)/cell_to_avg)))
    max_z = (int(np.floor((mz-1)/cell_to_avg)))
    if mx%cell_to_avg !=0:
      max_x-=1
    if my%cell_to_avg !=0:
          max_y-=1
    if mz%cell_to_avg !=0:
      max_z-=1
    nx_index=np.array([int(np.floor(xx/cell_to_avg)) for xx in xglob])
    ny_index=np.array([int(np.floor(yy/cell_to_avg)) for yy in yglob])
    nz_index=np.array([int(np.floor(zz/cell_to_avg)) for zz in zglob])
    #pdb.set_trace()
    ix=np.where(nx_index<=max_x)
    iy=np.where(ny_index<=max_y)
    iz=np.where(nz_index<=max_z)
    #pdb.set_trace()
    return nx_index[ix],ny_index[iy],nz_index[iz]

def correlateyposition(cell_num,my,cell_to_avg): #compute
    max_y=int(np.floor(my/cell_to_avg))
    y_center = (cell_to_avg*cell_num*2+cell_to_avg)/2.0
    if(y_center).is_integer():
      y_cent = int(y_center)
      print "y position of center:", y_cent
    else:
      print "center of cell is not at a DNS y position. Change your cell size and try again"
      print "y position of center:", y_center
    return y_cent, max_y
