subroutine simple(case_name,myid,nx,ny,nz,tmp,prs,phi)

   implicit none
   ! ---------------------------------------------------------------------------
   integer, parameter :: ngvars = 9 ! number of variables for phi
   integer,intent(in) :: nx, ny, nz, myid
   integer ::  mx, my, mz, npx, npy, npz,np,i,nn,myid_in
   integer :: ierror, ioerror, iounit,nthreads,  datasize
   character(len=10) :: case_name
   character(len=15) :: binfile
   real(8) :: avg_tmp, tmp_check
   real(8), dimension(1:nx,1:ny,1:nz,1:ngvars),intent(out) :: phi
   real(8), dimension(1:nx,1:ny,1:nz),intent(out)  :: tmp,prs
   ! ---------------------------------------------------------------------------
   select case( trim(case_name) )

      case ( 'Re1000p60' )
         binfile = 'restart134a.bin'       ! File name
         mx  = 480;  my = 530;  mz = 288   ! Total number of points
         npx =   8; npy =   2; npz =   8   ! Number of procs per direction
         !npneed = 128
         Tmp_check = 971.305337853206      ! Average temperature (check)

      case ( 'Re1000p60a' )
         binfile = 'restart136a.bin'       ! File name
         mx  = 480; my  = 530; mz  = 288   ! Total number of points
         npx =   8; npy =   2; npz =   8   ! Number of procs per direction
	 !npneed = 128
         Tmp_check = 1052.17544321744      ! Average temperature (check)

      case ( 'Re1000p80' )
         binfile = 'restart125a.bin'       ! File name
         mx  = 584; my  = 630; mz  = 344   ! Total number of points
         npx =   8; npy =   2; npz =   8   ! Number of procs per direction
	 !npneed = 128
         Tmp_check = 1019.61183300877      ! Average temperature (check)

      case ( 'Re2000p60' )
         binfile = 'restart171a.bin'       ! File name
         mx  = 768;  my = 804;  mz = 460   ! Total number of points
         npx =   8; npy =   6; npz =   5   ! Number of procs per direction
	 !npneed = 240
         Tmp_check = 964.670228886299      ! Average temperature (check)

      case default
         if (myid == 0) write(*,*) 'Case not found'
         !call MPI_BARRIER( MPI_COMM_WORLD, ierror )
         !call MPI_FINALIZE(ierror)

   end select


   ! calculate file size (integer=4 bytes, double precision=8 bytes)
   datasize = 4 + 8*( size(phi(1:nx,1:ny,1:nz,1:ngvars))  &
                    + size(prs(1:nx,1:ny,1:nz))           &
                    + size(tmp(1:nx,1:ny,1:nz)) )

    iounit = 100 + myid
     open( unit=iounit, file=binfile, status='old', iostat=ioerror  &
             , form='unformatted', access='direct', recl=datasize )


     if (myid == 0) write(*,*) 'Reading file.. ', trim(binfile)
     read( iounit, rec = myid+1 ) myid_in, phi(1:nx,1:ny,1:nz,1:ngvars)  &
                                       , prs(1:nx,1:ny,1:nz)           &
                                       , tmp(1:nx,1:ny,1:nz)

     close(iounit)


end subroutine simple
