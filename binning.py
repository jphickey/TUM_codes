from numpy import *
import pdb
import scipy
import source_term
import numpy as np
import mixtureFraction as mf
from scipy import stats

def binning(phi,cstar,nbins,statOpt='sum'):
  nx,ny,nz = np.shape(phi)
  phi      = phi.transpose(1,0,2).reshape(ny,-1)
  cstar    = cstar.transpose(1,0,2).reshape(ny,-1) #needs to change
  phi_sum  = np.zeros([ny,nbins])
  for ii in range(ny):
    phi_sum[ii,:], bin_edges, binnumber = stats.binned_statistic(cstar[ii,:],phi[ii,:],statistic=statOpt,bins = nbins,range=(0,1))  #range may need to be changed based on conditional variable
  return phi_sum


def double_binning(phi,cstar_1,cstar_2,cstar_2min,cstar_2max,nbins_1,nbins_2,statOpt='sum'):
  nx,ny,nz = np.shape(phi)
  phi      = phi.transpose(1,0,2).reshape(ny,-1)
  cstar_1    = cstar_1.transpose(1,0,2).reshape(ny,-1)
  cstar_2    = cstar_2.transpose(1,0,2).reshape(ny,-1)
  phi_sum  = np.zeros([ny,nbins_1,nbins_2])

  for ii in range(ny):
    phi_sum[ii,:,:], bin_edges1,bin_edges2, binnumber = stats.binned_statistic_2d(cstar_1[ii,:],cstar_2[ii,:],phi[ii,:],statistic=statOpt,bins =(nbins_1,nbins_2),range=((0,1),(cstar_2min,cstar_2max))) 

  return phi_sum

def normalize(phi_sum,cst):
  return phi_sum/float(cst)


def Merge_Cond_results(T_sums,rho_sums,Y1_sums,Y2_sums,c_nums,nbins):

  T_mean = T_sums/c_nums

  rho_mean = rho_sums/c_nums

  Y1_mean = Y1_sums/c_nums
  Y2_mean = Y2_sums/c_nums


  ctot = np.sum(c_nums,axis=1)
  P_c = np.zeros((len(ctot),nbins))
  for i in range(nbins):
    P_c[:,i] = c_nums[:,i]/ctot
  omega_c = source_term.calc_omega(T_mean,rho_mean,Y1_mean,Y2_mean)
  omega_c[c_nums==0]=0
  omega = sum(omega_c*P_c,axis=1)

  return omega

def Merge_results_assumption(omeg_sums,T_sums,rho_sums,Y1_sums,Y2_sums,Y3_sums,Y4_sums,c_nums,nbins,flag):

  T_mean = T_sums/c_nums

  rho_mean = rho_sums/c_nums

  Y1_mean = Y1_sums/c_nums
  Y2_mean = Y2_sums/c_nums
  Y3_mean = Y3_sums/c_nums
  Y4_mean = Y4_sums/c_nums
  omega_mean = omeg_sums/c_nums


  omega_c = source_term.calc_omega(T_mean,rho_mean,Y1_mean,Y2_mean)
  if  flag:
    omega_c = omega_c*source_term.molar_mass(Y1_mean,Y2_mean,Y3_mean,Y4_mean)*0.001

  omega_c[c_nums==0]=0
  omega_mean[c_nums==0]=0
  #omega = sum(omega_c,axis=1)

  return omega_c, omega_mean


def Merge_results_DCSE_assumption(omeg_sums,T_sums,rho_sums,Y1_sums,Y2_sums,Y3_sums,Y4_sums,c_nums,nbins1,nbins2,flag):
  
  T_mean = T_sums/c_nums
  rho_mean = rho_sums/c_nums
  Y1_mean = Y1_sums/c_nums
  Y2_mean = Y2_sums/c_nums
  Y3_mean = Y3_sums/c_nums
  Y4_mean = Y4_sums/c_nums
  omega_2d = omeg_sums/c_nums

  omega_mean = np.sum(omeg_sums,axis=2)/np.sum(c_nums,axis=2)


  T_cse = np.sum(T_sums,axis=2)/np.sum(c_nums,axis=2)
  rho_cse = np.sum(rho_sums,axis=2)/np.sum(c_nums,axis=2)
  Y1_cse = np.sum(Y1_sums,axis=2)/np.sum(c_nums,axis=2)
  Y2_cse = np.sum(Y2_sums,axis=2)/np.sum(c_nums,axis=2)
  Y3_cse = np.sum(Y3_sums,axis=2)/np.sum(c_nums,axis=2)
  Y4_cse = np.sum(Y4_sums,axis=2)/np.sum(c_nums,axis=2)

  omega_CSE = source_term.calc_omega(T_cse,rho_cse,Y1_cse,Y2_cse)

  c_all = np.sum(c_nums[:,:,:],axis=0)

  T_all = np.sum(T_sums[:,:,:],axis=0)/c_all
  rho_all = np.sum(rho_sums[:,:,:],axis=0)/c_all
  Y1_all = np.sum(Y1_sums[:,:,:],axis=0)/c_all
  Y2_all = np.sum(Y2_sums[:,:,:],axis=0)/c_all
  Y3_all = np.sum(Y3_sums[:,:,:],axis=0)/c_all
  Y4_all = np.sum(Y4_sums[:,:,:],axis=0)/c_all

  omega_all = np.sum(omeg_sums[:,:,:],axis=0)/c_all#source_term.calc_omega(T_all,rho_all,Y1_all,Y2_all)
  omega_all2 = source_term.calc_omega(T_all,rho_all,Y1_all,Y2_all)
  probs_all = np.sum(c_nums[:,:,:],axis=0)/np.sum(c_nums[:,:,:])
  omega_all[c_all == 0] = 0
  omega_all2[c_all == 0] = 0
  
  

  ctot = np.sum(c_nums,axis=(1,2))
  probs = np.zeros((len(ctot),nbins1))
  probs_DCSE = np.zeros((len(ctot),nbins1,nbins2))
  prob_both = np.zeros((len(ctot),nbins1,nbins2))

  omega_c = source_term.calc_omega(T_mean,rho_mean,Y1_mean,Y2_mean)
  if  flag:
    omega_c = omega_c*source_term.molar_mass(Y1_mean,Y2_mean,Y3_mean,Y4_mean)*0.001
    omega_CSE = omega_CSE*source_term.molar_mass(Y1_cse,Y2_cse,Y3_cse,Y4_cse)*0.001

  c_DCSE = np.sum(c_nums,axis=2)
  for i in range(nbins1):
    probs[:,i]= c_DCSE[:,i]/ctot
    for j in range(nbins2):
      probs_DCSE[:,i,j]= c_nums[:,i,j]/c_DCSE[:,i]
      prob_both[:,i,j] = c_nums[:,i,j]/np.sum(c_nums,axis=(1,2))


  omega_c[c_nums==0]=0

  omega_DCSE = omega_c
  probs_DCSE[c_nums==0]=0   
  omega_2d[c_nums==0]=0   
  omega_CSE[c_DCSE==0]=0
  #omega_DCSE[c_DCSE==0]=0
  omega_mean[c_DCSE==0]=0

  return omega_DCSE,omega_CSE,omega_mean,probs,probs_DCSE,prob_both,omega_2d,omega_all,omega_all2,probs_all


def Rans_condition(react,tmp,phi,fract1,fract2,fract3,fract4,nbins,ny,Yfuel_bot,Yoxy_top,mftype):
  #Obsolete Function?
  tmp = tmp.transpose(1,0,2).reshape(ny,-1)
  omeg = react.transpose(1,0,2).reshape(ny,-1)
  rho = phi.transpose(1,0,2).reshape(ny,-1)
  Y1= fract1.transpose(1,0,2).reshape(ny,-1) #needs to change
  Y2= fract2.transpose(1,0,2).reshape(ny,-1) #needs to change
  Y3= fract3.transpose(1,0,2).reshape(ny,-1) #needs to change
  Y4= fract4.transpose(1,0,2).reshape(ny,-1) #needs to change
  cstar = mf.computeMixtureFraction(mftype,Y1,Y2,Yfuel_bot,Yoxy_top,Y3,Y4)

  omega_sum = np.zeros((ny,nbins))
  T_sum = np.zeros((ny,nbins))
  rho_sum = np.zeros((ny,nbins))
  Y1_sum = np.zeros((ny,nbins))
  Y2_sum = np.zeros((ny,nbins))
  Y3_sum = np.zeros((ny,nbins))
  Y4_sum = np.zeros((ny,nbins))
  c_num = np.zeros((ny,nbins))

  for j in range(ny):
    omega_sum2 = np.zeros(nbins)
    T_sum2 = np.zeros(nbins)
    rho_sum2 = np.zeros(nbins)
    Y1_sum2 = np.zeros(nbins)
    Y2_sum2 = np.zeros(nbins)
    Y3_sum2 = np.zeros(nbins)
    Y4_sum2 = np.zeros(nbins)
    cnum2 = np.zeros(nbins)


    omega_sum2[:],T_sum2[:],rho_sum2[:],Y1_sum2[:],Y2_sum2[:],Y3_sum2[:],Y4_sum2[:],cnum2[:] = Cond_bin(omeg[j,:],tmp[j,:],rho[j,:],Y1[j,:],Y2[j,:],Y3[j,:],Y4[j,:],cstar[j,:],nbins)
    omega_sum2[:] = omega_sum2[:]/cnum2[:]
    T_sum[j,:] = T_sum2[:]/cnum2[:]
    rho_sum[j,:] = rho_sum2[:]/cnum2[:]
    Y1_sum[j,:] = Y1_sum2[:]/cnum2[:]
    Y2_sum[j,:] = Y2_sum2[:]/cnum2[:]
    Y3_sum[j,:] = Y3_sum2[:]/cnum2[:]
    Y4_sum[j,:] = Y4_sum2[:]/cnum2[:]
    omega_sum2[cnum2==0]=0
    T_sum[j,cnum2==0] = 0
    rho_sum[j,cnum2==0] = 0
    Y1_sum[j,cnum2==0] = 0
    Y2_sum[j,cnum2==0] = 0
    Y3_sum[j,cnum2==0] = 0
    Y4_sum[j,cnum2==0] = 0
    omega_sum[j,:] = omega_sum2[:]
  return omega_sum,T_sum,rho_sum,Y1_sum,Y2_sum,Y3_sum,Y4_sum,c_num
