import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from mpl_toolkits.mplot3d import Axes3D
from scipy import interpolate
from matplotlib import rc
import pickle
rc('text', usetex=True)
font = {'family' : 'sans-serif','sans-serif':['Helvetica'],'weight' : 'bold','size'   : 14}
rc('font', **font)

nbins1 =100
nbins2 =50

Xout = pickle.load( open( "Xout.p", "rb" ) )
Yout = pickle.load( open( "Yout.p", "rb" ) )
Zout = pickle.load( open( "Zout.p", "rb" ) )
plt.hist2d(Xout, Yout, bins=[nbins1,nbins2], weights=Zout,norm=colors.LogNorm(),cmap='binary')
plt.colorbar(label=r'$P(\zeta,T)$')
plt.xlabel(r'$\zeta$')
plt.ylabel(r'$T$ (K)')
plt.tight_layout()
plt.savefig("2D_prob_rho.pdf")
plt.show()
