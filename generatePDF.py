import numpy as np
import matplotlib.pyplot as plt
import pdb
import math
from mpi4py import MPI
from scipy import interpolate
from scipy.stats import beta
from scipy.stats import gamma
from scipy.stats import norm
from scipy.stats import invgauss
import scipy.special

def gammaPDF(a,b,x):
  return a*(a*x)**(b-1)*np.exp(-a*x)/scipy.special.gamma(b)

def convolution_PDF(pdfType, Fmean, Fvar, Omean,Ovar,YoxU,YfL,nbins,Zcomp=1.0):
  eps = 1E-8
  G2   = Fmean*(1.0-Fmean)/Fvar -1.0
  if G2 > eps:
    a2 = Fmean*G2
    b2 = G2-a2
    G1   = Omean*(1.0-Omean)/Ovar -1.0
    if G1 > eps:
      a1 = Omean*G1
      b1 = G1-a1
      mO2=15.99*2.0
      vO2=11.0
      mC7H16=100.21
      vC7H16=1.0
      v=vO2*mO2/(vC7H16*mC7H16)
      y = np.zeros((nbins,nbins+1))
      x = np.zeros((nbins,nbins+1))
      Z = np.linspace(1.0/nbins,1.0,nbins)
      myCDF = np.zeros(nbins+1)
      myPDF = np.zeros(nbins)
      for i in range(nbins):
        y[i,:] = np.linspace(0,YoxU,nbins+1)
        x[i,:] = (Z[i]*(v*YfL+YoxU)-YoxU+y[i,:])/v
        CDF_sum = 0
        for j in range(nbins):
          #if x[i,j]<YfL and x[i,j+1]>0:
          CDF_sum = CDF_sum + (beta.cdf(y[i,j+1]/YoxU, a1, b1)-beta.cdf(y[i,j]/YoxU, a1, b1))*(beta.cdf(x[i,j+1]/YfL, a2, b2)+beta.cdf(x[i,j]/YfL, a2, b2))/2
        myCDF[i+1] = CDF_sum
        myPDF[i]   = (myCDF[i+1]-myCDF[i])*float(nbins)
    else:
      myPDF = np.zeros(nbins)
  else:
    myPDF = np.zeros(nbins)
  return myPDF
#  Z=(v*Yfuel-Yoxy+YoxU)/(v*YfL+YoxU)

def compute_PDF(pdfType, Zmean, Zvar,nbins,Zcomp=1.0):
  myPDF = np.zeros(nbins)
  myCDF = np.zeros(nbins+1)
  if pdfType=="beta":
    eps = 1E-8
    ZM  = np.linspace(0,1,nbins+1)
    G   = Zmean*(1.0-Zmean)/Zvar -1.0
    if G > eps:
      a = Zmean*G
      b = G-a
      myCDF = beta.cdf(ZM, a, b)
    else:
      myPDF=np.zeros(nbins)
      print "zero!"
    for i in range(nbins):
      myPDF[i] = (myCDF[i+1]-myCDF[i])*float(nbins)
  if pdfType=="norm":
    ZM  = np.linspace(0,1,nbins+1)
    myCDF = norm.cdf(ZM,Zmean,Zvar**0.5)
    for i in range(nbins):
      myPDF[i] = (myCDF[i+1]-myCDF[i])*float(nbins)
  if pdfType=="wald":
    ZM  = np.linspace(0,1,nbins+1)
    myCDF = invgauss.cdf(ZM,Zmean)
    for i in range(nbins):
      myPDF[i] = (myCDF[i+1]-myCDF[i])*float(nbins)

  if pdfType=="invgauss":
    ZM  = np.linspace(0.5/nbins,1.0-0.5/nbins,nbins)
    mu = Zmean
    lda = mu**3/Zvar
    for i in range(nbins):
      myPDF[i] = (lda/(2*math.pi*ZM[i]**3))**0.5*np.exp(-lda*(ZM[i]-mu)**2/(2*mu**2*ZM[i]))

  elif pdfType=='gamma':
    ZM  = np.linspace(0,1,nbins)
    a=Zmean/Zvar
    b=Zmean**2/Zvar
    myPDF=gammaPDF(a,b,ZM)
  elif pdfType=='doubleGamma':
    eps=1.0-Zcomp
    ZM  = np.linspace(0,1,nbins)
    a=Zmean/Zvar
    b=Zmean**2/Zvar
    a1   = Zcomp/Zvar
    b1   = Zcomp**2/Zvar
    myPDF= (1-eps)*gammaPDF(a,b,ZM)+eps*gammaPDF(a1,b1,ZM)
  return myPDF


def compute_error(est,anal):
  err=(est-anal)**2
  return np.sqrt(np.mean(err))
