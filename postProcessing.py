
import numpy as np
import matplotlib.pyplot as plt
import mixtureFraction as mf
import source_term as src
import simple
import time
import binning
import pdb
import tool
from mpl_toolkits.mplot3d import Axes3D
from mpi4py import MPI
from scipy import interpolate
from matplotlib import rc
import generatePDF as distribution
import pickle
rc('text', usetex=True)
font = {'family' : 'sans-serif','sans-serif':['Helvetica'],'weight' : 'bold','size'   : 14}
rc('font', **font)

comm = MPI.COMM_WORLD
rank = MPI.COMM_WORLD.Get_rank()
size = MPI.COMM_WORLD.Get_size()
name = MPI.Get_processor_name()
print rank


############################
# USER-INPUT
############################
case        = 'Re2000p60'        # 'baseline'  name of case
mergeCells  = 4                 # nb cells for grid coarsing
MFtype      =  "fuelair"        # definition of the mixture fraction "fuelair"/"bilger"
nbAvgVar    = 13                # nb variables for the averaging list
pdfType     = "beta"
pdfType2    = "beta"
nbins1       = 100  # 1st condition variable
nbins2       = 50  # 2nd conditioning variable (for DCSE)
condition_var2  = "tmp" # choose: "tmp" (temperature),"rho"(density),"Zcomp"(compressibility),"YCO2"(Y_co2),"prs"(pressure)
y_sect = 465
RANS_height = 10

compute_statistics = True       # compute the statistics
compute_RANS       = False      # compute the grid coarsening
compute_single_condition  = True # compute the conditional sampling
compute_double_condition  = True  # compute the double conditional sampling
compute_flameloc   = True      # compute flame location

convert_omega_to_kg        = False #CAUTION! not fully implemented yet
scatter_plt_CSE_assumption = False
plot_avg_omega_by_y        = False
RANS_conditioning          = False
plot_PDFS                  = False
compute_PDF_2D             = True

#global dictionary
R  = {"rho":0,"tmp":1,"prs":2,"YC7H16":3,"YO2":4,"omega":5,"filtered_omega":6,"U":7,"rhoU":8,"YCO2":9,"YH2O":10,"Z":11,"Zcomp":12}


############################
# SETUP CASES
############################
sp={'C7H16':8,"O2":7,"CO2":6,"H2O":5}  # dictionary of species
if case=='Re1000p60':
  binfile = 'restart134a.bin'          # File name
  mx  = 480;  my = 530;  mz = 288      # Total number of points
  npx =   8; npy =   2; npz =   8      #! Number of procs per direction
  npneed = 128
  nx = mx/npx; ny = my/npy; nz = mz/npz
  Tmp_check = 971.305337853206      # Average temperature (check)
  Yoxy_top  = 0.2
  Yfuel_bot = 0.955
  y_flame    = 388   #precomputed
  y_zero     = 212   #precomputed
  tmax=2572.0 #precomputed
  tmin=594.0  #precomputed
  pmax =7438046.0  #precomputed
  pmin = 5925293.0   #precomputed
  rhomax = 237.0    #precomputed
  rhomin = 8.0    #precomputed
  y_sections   = 11   # number of sections to divide the domain
  y_sect_index = [np.arange(0,100),np.arange(100,200),np.arange(200,300),np.arange(250,300),np.arange(300,325),np.arange(325,350),np.arange(350,375),np.arange(375,400),np.arange(400,425),np.arange(425,450),np.arange(450,500)]
  vort_thick=0.00700102799095 #precomputed
  L2        = 0.221   #m
  x2        = np.linspace(-L2/2.5, L2-L2/2.5,my)
  x2_vort   = np.linspace(-L2/2.5/vort_thick, (L2-L2/2.5)/vort_thick,my)
elif case== 'Re1000p60a':
  binfile = 'restart136a.bin'
  mx  = 480; my  = 530; mz  = 288
  npx =   8; npy =   2; npz =   8
  npneed = 128
  nx = mx/npx; ny = my/npy; nz = mz/npz
  Tmp_check = 1052.17544321744
  Yoxy_top  = 0.245
  Yfuel_bot = 1.0
  y_flame    = 388   #precomputed
  y_zero     = 212   #precomputed
  tmax=2800.0 #precomputed
  tmin=594.0  #precomputed
  pmax =7438046.0  #precomputed
  pmin = 5925293.0   #precomputed
  rhomax = 237.0    #precomputed
  rhomin = 8.0    #precomputed
  y_sections   = 11   # number of sections to divide the domain
  y_sect_index = [np.arange(0,100),np.arange(100,200),np.arange(212,300),np.arange(300,325),np.arange(325,350),np.arange(350,375),np.arange(375,400),np.arange(400,425),np.arange(425,450),np.arange(450,500),np.arange(500,my)]
  vort_thick=0.00700102799095 #precomputed
  L2        = 0.221   #m
  x2        = np.linspace(-L2/2.5, L2-L2/2.5,my)
  x2_vort   = np.linspace(-L2/2.5/vort_thick, (L2-L2/2.5)/vort_thick,my)
elif case== 'Re1000p80':
  binfile = 'restart125a.bin'
  mx  = 584; my  = 630; mz  = 344
  npx =   8; npy =   2; npz =   8 
  npneed = 128
  nx = mx/npx; ny = my/npy; nz = mz/npz
  Tmp_check = 1019.61183300877
  Yoxy_top  = 0.2
  Yfuel_bot = 0.955
  y_flame    = 388   #precomputed
  y_zero     = 212   #precomputed
  tmax=3300.0 #precomputed
  tmin=590.0  #precomputed
  pmax =7438046.0  #precomputed
  pmin = 5925293.0   #precomputed
  rhomax = 237.0    #precomputed
  rhomin = 8.0    #precomputed
  y_sections   = 11   # number of sections to divide the domain
  y_sect_index = [np.arange(0,100),np.arange(100,200),np.arange(212,300),np.arange(300,325),np.arange(325,350),np.arange(350,375),np.arange(375,400),np.arange(400,425),np.arange(425,450),np.arange(450,500),np.arange(500,my)]
  vort_thick=0.00700102799095 #precomputed
  L2        = 0.221   #m
  x2        = np.linspace(-L2/2.5, L2-L2/2.5,my)
  x2_vort   = np.linspace(-L2/2.5/vort_thick, (L2-L2/2.5)/vort_thick,my)
elif case== 'Re2000p60':
  binfile = 'restart171a.bin'
  mx  = 768;  my = 804;  mz = 460
  npx =   8; npy =   6; npz =   5
  npneed = 240
  nx = mx/npx; ny = my/npy; nz = mz/npz
  Tmp_check = 964.670228886299
  Yoxy_top  = 0.2
  Yfuel_bot = 0.955
  y_flame    = 388   #precomputed
  y_zero     = 212   #precomputed
  tmax=3300.0 #precomputed
  tmin=590.0  #precomputed
  pmax =7438046.0  #precomputed
  pmin = 5925293.0   #precomputed
  rhomax = 237.0    #precomputed
  rhomin = 8.0    #precomputed
  y_sections   = 11   # number of sections to divide the domain
  y_sect_index = [np.arange(0,100),np.arange(100,200),np.arange(212,300),np.arange(300,325),np.arange(325,350),np.arange(350,375),np.arange(375,400),np.arange(400,425),np.arange(425,450),np.arange(450,500),np.arange(500,my)]
  vort_thick=0.00700102799095 #precomputed
  L2        = 0.221   #m
  x2        = np.linspace(-L2/2.5, L2-L2/2.5,my)
  x2_vort   = np.linspace(-L2/2.5/vort_thick, (L2-L2/2.5)/vort_thick,my)
else:
  print"not yet made"
  quit()
pickle.dump(x2_vort, open( "x2_vort.p", "wb" ) )
print x2_vort[212],x2_vort[300]
#Orders the blocks according to python ordering scheme
order=(np.arange(npneed).reshape((npx,npy,npz)))

#Setup the empty arrays for statistics
maximum         = np.zeros([nbAvgVar])
max_total       = np.copy(maximum)
maxProfile      = np.zeros([my,nbAvgVar])
maxProfile_total= np.copy(maxProfile)
avg             = np.zeros([nbAvgVar])
avg_total       = np.copy(avg)
avgProfile      = np.zeros([my,nbAvgVar])
avgProfile_total= np.copy(avgProfile)
avgVar          = np.zeros([my,nbAvgVar])
avgVar_total    = np.copy(avgProfile)
PDF_error       = np.zeros([y_sections])

#Conditioning variables
condition        = np.zeros([my,nbins1,nbAvgVar])
condition_total  = np.zeros([my,nbins1,nbAvgVar])
condCount        = np.zeros([my,nbins1])
condCount_total  = np.zeros([my,nbins1])
condMean_total   = np.zeros([my,nbins1,nbAvgVar])
condVar          = np.zeros([my,nbins1,nbAvgVar])
condVar_total    = np.zeros([my,nbins1,nbAvgVar])
PDF              = np.zeros([my,nbins1,nbAvgVar])
zCompCount       = np.zeros([my,nbins1])  #finds where the most occurances of varioud
zCompCount_total = np.zeros([my,nbins1])


#Double Conditioning variables
if compute_double_condition:
  condition2        = np.zeros([my,nbins1,nbins2,nbAvgVar])
  condition2_total  = np.zeros([my,nbins1,nbins2,nbAvgVar])
  condCount2        = np.zeros([my,nbins1,nbins2])
  condCount2_total  = np.zeros([my,nbins1,nbins2])
  condMean2_total   = np.zeros([my,nbins1,nbins2,nbAvgVar])
  condVar2          = np.zeros([my,nbins1,nbins2,nbAvgVar])
  condVar2_total    = np.zeros([my,nbins1,nbins2,nbAvgVar])
  PDF2              = np.zeros([my,nbins1,nbins2,nbAvgVar])
if condition_var2 == "tmp":
  cstar2max = tmax
  cstar2min = tmin
elif condition_var2 == "rho":
  cstar2max = rhomax
  cstar2min = rhomin
elif condition_var2 == "Zcomp":
  cstar2max = 1.2
  cstar2min = 0.8
elif condition_var2 == "YCO2":
  cstar2max = 1.0
  cstar2min = 0.0
elif condition_var2 == "prs":
  cstar2max = pmax
  cstar2min = pmin
elif condition_var2 == "YO2":
  cstar2max = 1.0
  cstar2min = 0.0
elif condition_var2 == "omega":
  cstar2max = 1000000
  cstar2min = 0
else:
  print "Conditioning variable not implemented"

#Generate a coarse mesh (global variable)
RANS_total = None  # needed for the parallel processing
temp       = tool.createCoarse(mx,my,mz,mergeCells)
RANS       = [temp,temp,temp,temp,temp,temp,temp,temp,temp,temp,temp,temp,temp,temp,temp]
RANS_total = np.copy(RANS)

#==============================
#Make parallel
#==============================
if size ==1 : #not in parallel
  boxes=range(npneed)
else:
  box_per_proc=int(npneed/size)
  if npneed%size!=0:
      print "must perfectly divide the npneed per mpi processes"
      quit()
  boxes=np.arange(rank*box_per_proc,(rank+1)*box_per_proc)
  # only processor 0 will actually get the data

############################################################
############################################################
#     MAIN CODE
############################################################
############################################################
for myid in boxes:
  #Order of the blocks and global grid
  [xglob,yglob,zglob,bi,bj,bk]   = tool.globalCoord(myid,order,nx,ny,nz)

  #Read the database
  tmp,prs,phi = simple.simple(case,myid,nx,ny,nz)
  rho    = phi[:,:,:,0]
  YC7H16 = phi[:,:,:,sp['C7H16']]/rho
  YO2    = phi[:,:,:,sp['O2']]/rho
  YCO2   = phi[:,:,:,sp['CO2']]/rho
  YH2O   = phi[:,:,:,sp['H2O']]/rho
  U      = phi[:,:,:,1]/rho
  del phi

  #Compute source-term
  omega        = src.calc_omega(tmp,rho,YC7H16,YO2)

  #Compute molar mass
  mixture_mass = src.molar_mass(YC7H16,YO2,YCO2,YH2O)

  #Compute mixtureFraction
  Z   = mf.computeMixtureFraction(MFtype,YC7H16,YO2,Yfuel_bot,Yoxy_top,YCO2,YH2O)

  # Compute the compressibility factor
  Zcomp = tool.compute_Zcomp(tmp,prs,rho,mixture_mass)


  if compute_single_condition:
    # sum up all conditioned variables
    condition[yglob,:,R["tmp"]]    = condition[yglob,:,R["tmp"]]   + binning.binning(tmp,Z,nbins1)
    condition[yglob,:,R["rho"]]    = condition[yglob,:,R["rho"]]   + binning.binning(rho,Z,nbins1)
    condition[yglob,:,R["omega"]]  = condition[yglob,:,R["omega"]] + binning.binning(omega,Z,nbins1)
    condition[yglob,:,R["Z"]]      = condition[yglob,:,R["Z"]]     + binning.binning(Z,Z,nbins1)
    condition[yglob,:,R["Zcomp"]]  = condition[yglob,:,R["Zcomp"]] + binning.binning(Zcomp,Z,nbins1)

    #computing the mean conditioning (no need all of them as we count beans)
    condCount[yglob,:]       = condCount[yglob,:]      + binning.binning(Z,Z,nbins1,'count')
    zCompCount[yglob,:]      = zCompCount[yglob,:]      + binning.binning(Z,Zcomp,nbins1,'count')

    #computing the Variance
    condVar[yglob,:,R["Z"]]       = condVar[yglob,:,R["Z"]]      + binning.binning(np.square(Z),Z,nbins1)
    condVar[yglob,:,R["tmp"]]     = condVar[yglob,:,R["tmp"]]    + binning.binning(np.square(tmp),Z,nbins1)
    condVar[yglob,:,R["rho"]]     = condVar[yglob,:,R["rho"]]    + binning.binning(np.square(rho),Z,nbins1)
    condVar[yglob,:,R["Zcomp"]]   = condVar[yglob,:,R["Zcomp"]]  + binning.binning(np.square(Zcomp),Z,nbins1)
    condVar[yglob,:,R["omega"]]   = condVar[yglob,:,R["omega"]]  + binning.binning(np.square(omega),Z,nbins1)

  if compute_double_condition:
    # sum up all conditioned variables
    start1 = time.time()
    if condition_var2 == "tmp":
      cstar2 = tmp#tool.tstar(tmp,Z)
    elif condition_var2 == "rho":
      cstar2 = rho
    elif condition_var2 == "Zcomp":
      cstar2 = Zcomp
    elif condition_var2 == "YCO2":
      cstar2 = YCO2
    elif condition_var2 == "YO2":
      cstar2 = YO2
    elif condition_var2 == "prs":
      cstar2 = prs
    elif condition_var2 == "omega":
      cstar2 = omega
    condition2[yglob,:,:,R["tmp"]]    = condition2[yglob,:,:,R["tmp"]]   + binning.double_binning(tmp,Z,cstar2,cstar2min,cstar2max,nbins1,nbins2)
    condition2[yglob,:,:,R["rho"]]    = condition2[yglob,:,:,R["rho"]]   + binning.double_binning(rho,Z,cstar2,cstar2min,cstar2max,nbins1,nbins2)
    condition2[yglob,:,:,R["YC7H16"]]    = condition2[yglob,:,:,R["YC7H16"]]   + binning.double_binning(YC7H16,Z,cstar2,cstar2min,cstar2max,nbins1,nbins2)
    condition2[yglob,:,:,R["YO2"]]    = condition2[yglob,:,:,R["YO2"]]   + binning.double_binning(YO2,Z,cstar2,cstar2min,cstar2max,nbins1,nbins2)
    condition2[yglob,:,:,R["YCO2"]]    = condition2[yglob,:,:,R["YCO2"]]   + binning.double_binning(YCO2,Z,cstar2,cstar2min,cstar2max,nbins1,nbins2)
    condition2[yglob,:,:,R["YH2O"]]    = condition2[yglob,:,:,R["YH2O"]]   + binning.double_binning(YH2O,Z,cstar2,cstar2min,cstar2max,nbins1,nbins2)
    condition2[yglob,:,:,R["omega"]]  = condition2[yglob,:,:,R["omega"]] + binning.double_binning(omega,Z,cstar2,cstar2min,cstar2max,nbins1,nbins2)
    condition2[yglob,:,:,R["Z"]]      = condition2[yglob,:,:,R["Z"]]     + binning.double_binning(Z,Z,cstar2,cstar2min,cstar2max,nbins1,nbins2)
    condition2[yglob,:,:,R["Zcomp"]]  = condition2[yglob,:,:,R["Zcomp"]] + binning.double_binning(Zcomp,Z,cstar2,cstar2min,cstar2max,nbins1,nbins2)

    #computing the mean conditioning (no need all of them as we count beans)
    condCount2[yglob,:,:]       = condCount2[yglob,:,:]      + binning.double_binning(Z,Z,cstar2,cstar2min,cstar2max,nbins1,nbins2,'count')

    #computing the Variance
    condVar2[yglob,:,:,R["Z"]]       = condVar2[yglob,:,:,R["Z"]]      + binning.double_binning(np.square(Z),Z,cstar2,cstar2min,cstar2max,nbins1,nbins2)
    condVar2[yglob,:,:,R["tmp"]]       = condVar2[yglob,:,:,R["tmp"]]      + binning.double_binning(np.square(tmp),Z,cstar2,cstar2min,cstar2max,nbins1,nbins2)
    condVar2[yglob,:,:,R["rho"]]       = condVar2[yglob,:,:,R["rho"]]      + binning.double_binning(np.square(rho),Z,cstar2,cstar2min,cstar2max,nbins1,nbins2)
    end1 = time.time()
    print "DCSE binning time:",end1-start1

  if compute_statistics:
    avg[R["tmp"]]                 = avg[R["tmp"]]+ tool.avg(tmp)
    avg[R["rho"]]                 = avg[R["rho"]]+ tool.avg(rho)
    avgProfile[yglob,R["Z"]]     += tool.avg_y(Z)
    avgProfile[yglob,R["Zcomp"]] += tool.avg_y(Zcomp)
    avgProfile[yglob,R["omega"]] += tool.avg_y(omega)
    avgProfile[yglob,R["tmp"]]   += tool.avg_y(tmp)
    avgProfile[yglob,R["U"]]     += tool.avg_y(U)
    avgProfile[yglob,R["rho"]]   += tool.avg_y(rho)
    avgProfile[yglob,R["rhoU"]]  += tool.avg_y(rho*U)
    avgProfile[yglob,R["YC7H16"]]+= tool.avg_y(YC7H16)
    avgProfile[yglob,R["YO2"]]   += tool.avg_y(YO2)
    avgProfile[yglob,R["YCO2"]]   += tool.avg_y(YCO2)
    avgProfile[yglob,R["prs"]]   += tool.avg_y(prs)
    avgVar[yglob,R["rho"]]       += tool.avg_sqr_y(rho)
    avgVar[yglob,R["tmp"]]       += tool.avg_sqr_y(tmp)
    avgVar[yglob,R["Z"]]         += tool.avg_sqr_y(Z)
    avgVar[yglob,R["Zcomp"]]         += tool.avg_sqr_y(Zcomp)
    avgVar[yglob,R["YCO2"]]         += tool.avg_sqr_y(YCO2)
    avgVar[yglob,R["omega"]]     += tool.avg_sqr_y(omega)
    avgVar[yglob,R["YO2"]]     += tool.avg_sqr_y(YO2)
    avgVar[yglob,R["YC7H16"]]     += tool.avg_sqr_y(YC7H16)
    avgVar[yglob,R["prs"]]     += tool.avg_sqr_y(prs)
    maximum[R["omega"]]           = tool.max(omega)
    maxProfile[yglob,R["omega"]]  = tool.max_y(omega)


  if compute_RANS:
    #Populate the coarse mesh (Watch memory usage!!!)
    nx_index,ny_index,nz_index = tool.getFilter(mergeCells, xglob,yglob,zglob,mx,my,mz)
    RANS[R["prs"]]    = RANS[R["prs"]]   + tool.filterData_cell(prs,np.shape(temp) ,nx_index,ny_index,nz_index)
    RANS[R["omega"]]  = RANS[R["omega"]] + tool.filterData_cell(omega,np.shape(temp) ,nx_index,ny_index,nz_index)
    RANS[R["tmp"]]    = RANS[R["tmp"]]   + tool.filterData_cell(tmp,np.shape(temp),nx_index,ny_index,nz_index)
    RANS[R["rho"]]    = RANS[R["rho"]]   + tool.filterData_cell(rho,np.shape(temp) ,nx_index,ny_index,nz_index)
    RANS[R["YC7H16"]] = RANS[R["YC7H16"]]+ tool.filterData_cell(YC7H16,np.shape(temp),nx_index,ny_index,nz_index)
    RANS[R["YO2"]]    = RANS[R["YO2"]]   + tool.filterData_cell(YO2,np.shape(temp),nx_index,ny_index,nz_index)
    RANS[R["YCO2"]]   = RANS[R["YCO2"]]  + tool.filterData_cell(YCO2,np.shape(temp),nx_index,ny_index,nz_index)
    RANS[R["YH2O"]]   = RANS[R["YH2O"]]  + tool.filterData_cell(YH2O,np.shape(temp),nx_index,ny_index,nz_index)

  #output file number
  print "finished reading:", myid
# END main code loop
#########################

#Communicate back to base
if size>1:
    comm.Reduce(avg, avg_total, op=MPI.SUM, root=0)
    comm.Reduce(maximum, max_total, op=MPI.MAX, root=0)
    comm.Reduce(maxProfile, maxProfile_total, op=MPI.MAX, root=0)
    comm.Reduce(avgProfile, avgProfile_total, op=MPI.SUM, root=0)
    comm.Reduce(avgVar,     avgVar_total, op=MPI.SUM, root=0)
    for ii in range(len(RANS_total)):
      comm.Reduce(RANS[ii], RANS_total[ii], op=MPI.SUM, root=0)

    comm.Reduce(condition, condition_total, op=MPI.SUM, root=0)
    comm.Reduce(condCount, condCount_total, op=MPI.SUM, root=0)
    comm.Reduce(condVar,   condVar_total, op=MPI.SUM, root=0)
    comm.Reduce(condCount, condCount_total, op=MPI.SUM, root=0)
    comm.Reduce(zCompCount, zCompCount_total, op=MPI.SUM, root=0)
    if compute_double_condition:
      comm.Reduce(condition2, condition2_total, op=MPI.SUM, root=0)
      comm.Reduce(condCount2, condCount2_total, op=MPI.SUM, root=0)
      comm.Reduce(condVar2,   condVar2_total, op=MPI.SUM, root=0)

else:
  avg_total       = np.copy(avg)
  avgProfile_total= np.copy(avgProfile)
  avgVar_total    = np.copy(avgVar)
  RANS_total      = np.copy(RANS)
  condition_total = np.copy(condition)
  condCount_total  = np.copy(condCount)
  zCompCount_total  = np.copy(zCompCount)
#print "done merging", rank
del RANS

############################################################
##After summing all the values, we divide by number of cells:
############################################################
if rank==0:
  ###########################################
  print "+++ Working on one core +++"
  ############################
  #Normalize all statistics
  ############################
  avg_total        /= float(npneed)
  avgProfile_total /= float(npneed/npy)
  avgVar_total      = avgVar_total/float(npneed/npy) - np.square(avgProfile_total)
  for ii in range(len(RANS_total)):
    RANS_total[ii] /= float(mergeCells**3)

  ###########################################
  #Verification block
  ###########################################
  print "======================================"
  print "Computed: ", avg_total[R["tmp"]]
  print "Exact   : ", Tmp_check
  print "Avg'd blocks: ",tool.avg(RANS_total[R["tmp"]])
  print "density: ",avg_total[R["rho"]],tool.avg(RANS_total[R["rho"]])
  print "======================================"

  #Compute source term based on mean values
  if compute_RANS:
    RANS_total[R["filtered_omega"]] = src.calc_omega(RANS_total[R["tmp"]],RANS_total[R["rho"]],RANS_total[R["YC7H16"]],RANS_total[R["YO2"]])


  ###########################################
  #Compute the conditioning
  ###########################################
  if compute_single_condition:
    for ii in range(nbAvgVar):
      PDF[:,:,ii]         = binning.normalize(condition_total[:,:,ii],mx*mz)
    #Create a special variable for Z
    PDF_Z = binning.normalize(condCount_total[:,:],mx*mz)
    cindex= condCount_total[:,:]>0
    for ii in range(nbAvgVar):
      condMean_total[cindex,ii]  = condition_total[cindex,ii]/condCount_total[cindex]
      condVar_total[cindex,ii]  = condVar_total[cindex,ii]/condCount_total[cindex] - np.square(condMean_total[cindex,ii])
    PDF_global       = np.zeros([nbins1,nbAvgVar])
    PDF_sections     = np.zeros([y_sections,nbins1,nbAvgVar])
    PDF_Var_sections = np.zeros([y_sections,nbins1,nbAvgVar])
    Count_sections   = np.zeros([y_sections,nbins1,nbAvgVar])
    PDF_Z_sections   = np.zeros([y_sections,nbins1])
    PDF_Z_global     = np.sum(PDF_Z, axis=0)/float(my)
    MF_Zcomp_global  =np.sum(PDF_Z, axis=0)/float(my)
    for jj in range(y_sections):
      temp=PDF_Z[y_sect_index[jj][0]:y_sect_index[jj][-1],:]
      PDF_Z_sections[jj,:] = np.sum(temp, axis=0)/float(len(y_sect_index[jj]))
    for ii in range(nbAvgVar):
      PDF_global[:,ii] = np.sum(condMean_total[:,:,ii], axis=0)/float(my)
    for ii in range(nbAvgVar):
      for jj in range(y_sections):
        aa        = y_sect_index[jj][0]
        bb        = y_sect_index[jj][-1]
        PDF_sections[jj,:,ii]     = np.sum(condMean_total[aa:bb,:,ii], axis=0)/float(len(y_sect_index[jj]))
        PDF_Var_sections[jj,:,ii] = np.mean(condVar_total[aa:bb,:,ii], axis=0)/float(len(y_sect_index[jj]))

   # Compute the error from the PDF
    for jj in range(y_sections):
      aa        = y_sect_index[jj][0]
      bb        = y_sect_index[jj][-1]
      mean_sec  = np.mean(avgProfile_total[aa:bb,R["Z"]])
      var_sec   = np.mean(avgVar_total[aa:bb,R["Z"]])
      mypdf     = distribution.compute_PDF(pdfType, mean_sec, var_sec,nbins1)
   #   PDF_error[jj] = distribution.compute_error(PDF_Z_sections,mypdf)
    mO2=15.99*2.0
    vO2=11.0
    mC7H16=100.21
    vC7H16=1.0
    v=vO2*mO2/(vC7H16*mC7H16)
    for jj in range(0,y_sections):
      aa        = y_sect_index[jj][0]
      bb        = y_sect_index[jj][-1]
      mean_sec  = avgProfile_total[aa,R["Z"]]
      var_sec   = avgVar_total[aa,R["Z"]]
      mypdf     = distribution.compute_PDF(pdfType, mean_sec, var_sec,nbins1)
      print "Mean,Variance zeta:", mean_sec,var_sec
      mypdf = mypdf/np.sum(mypdf)
      Omean  = avgProfile_total[aa,R["YO2"]]/Yoxy_top
      Ovar   = avgVar_total[aa,R["YO2"]]/Yoxy_top/Yoxy_top
      mypdf2     = distribution.compute_PDF(pdfType, Omean, Ovar,nbins1)
      print "Mean,Variance YO2:", Omean,Ovar
      mypdf2 = mypdf2/np.sum(mypdf2)
      Fmean    = avgProfile_total[aa,R["YC7H16"]]/Yfuel_bot
      Fvar     = avgVar_total[aa,R["YC7H16"]]/Yfuel_bot/Yfuel_bot
      mypdf3   = distribution.compute_PDF(pdfType, Fmean, Fvar,nbins1)
      print "pdf fuel sum:",np.sum(mypdf3)
      mypdf3 = mypdf3/np.sum(mypdf3)
      print "variance z, yo, yf:",avgVar_total[aa,R["Z"]],avgVar_total[aa,R["YO2"]],avgVar_total[aa,R["YC7H16"]]
      print "Covariance:",(avgVar_total[aa,R["Z"]]-avgVar_total[aa,R["YC7H16"]]*(v/(v*Yfuel_bot+Yoxy_top))*(v/(v*Yfuel_bot+Yoxy_top))-avgVar_total[aa,R["YO2"]]*(1/(v*Yfuel_bot+Yoxy_top))*(1/(v*Yfuel_bot+Yoxy_top)))*(-((v*Yfuel_bot+Yoxy_top)*(v*Yfuel_bot+Yoxy_top))/(2*v))
      pdfconv  = distribution.convolution_PDF(pdfType, Fmean, Fvar, Omean,Ovar,Yoxy_top,Yfuel_bot,nbins1)
      print"pdfconv sum:",np.sum(pdfconv)
      pdfconv  = pdfconv/np.sum(pdfconv)
      if plot_PDFS:
        print "Mean,Variance Yfuel:", Fmean,Fvar
        print "x2/sigma: ",x2_vort[aa]
        #plt.plot(np.linspace(0,1,nbins1),PDF_Z_sections[jj,:]*nbins1,label='whole block')
        plt.plot(np.linspace(0,1,nbins1),PDF_Z[aa],label='DNS-PDF')
        #plt.plot(np.linspace(0,1,nbins1),PDF_Z[bb]*nbins1,label='top of block')
        plt.plot(np.linspace(0,1,nbins1),mypdf,label='Beta')
        #plt.plot(np.linspace(0,1,nbins1),mypdf2,label='fitted function, YO2')
        #plt.plot(np.linspace(0,1,nbins1),mypdf3,label='fitted function, YC7H16')
        plt.plot(np.linspace(0,1,nbins1),pdfconv,label='Beta convolution')
        # plt.plot(np.linspace(0,1,nbins1),PDF_sections[jj,:,R["Zcomp"]]*nbins1/float(mx*mz))
        plt.legend()
        plt.show()
      #plt.plot(np.linspace(0,1,nbins1),PDF_Z_global*PDF_global[:,R["Zcomp"]])
      #plt.plot(np.linspace(0,1,nbins1),PDF_Var_sections[jj,:,R["omega"]]/PDF_sections[jj,:,R["omega"]],label=str(jj))
   #   plt.plot(np.linspace(0,1,nbins1),PDF_Var_sections[jj,:,R["omega"]]/PDF_sections[jj,:,R["omega"]]*nbins1)


  #plt.plot(np.linspace(0,1,nbins1),PDF_global[:,5]/max(PDF_global[:,5]))
  #plt.plot(np.linspace(0,1,nbins1),PDF_Z_global/max(PDF_Z_global))
    #plt.legend()
    #plt.show()


  ###########################################
  #Computes the flame location
  ###########################################
  if compute_flameloc:
    dx     = L2/float(my)
    dU0    = np.abs(avgProfile_total[-1,R["U"]]-avgProfile_total[0,R["U"]])
    dU_dx2 = max(np.abs(avgProfile_total[1:,R["U"]]-avgProfile_total[:-1,R["U"]])/(dx))
    vort_thick = dU0 / dU_dx2
   #mid    = 0.5*(avgProfile_total[-1,R["rhoU"]]-avgProfile_total[0,R["rhoU"]])+avgProfile_total[0,R["rhoU"]]
   # mid_index=np.argmin(abs(avgProfile_total[:,R["rhoU"]]-mid*np.ones(len(ycoord))))
    print "mid_index",vort_thick


  if compute_RANS:
    RANS_Z = mf.computeMixtureFraction(MFtype,RANS_total[R["YC7H16"]],RANS_total[R["YO2"]],Yfuel_bot,Yoxy_top,RANS_total[R["YCO2"]],RANS_total[R["YH2O"]])

  if RANS_conditioning:
    nx_rans,ny_rans,nz_rans = np.shape(tool.createCoarse(mx,my,mz,mergeCells))
    RANS_cond               = np.zeros((nbAvgVar,ny_rans,nbins1,nbins2))
    RANS_omega              = np.zeros((ny_rans,nbins1,nbins2))
    RANS_omega_1D           = np.zeros((ny_rans,nbins1))
    RANS_prob               = np.zeros((ny_rans,nbins1,nbins2))
    RANS_prob_1D            = np.zeros((ny_rans,nbins1))
    RANS_CSE                = np.zeros(ny_rans)
    RANS_DCSE               = np.zeros(ny_rans)
    RANS_DNS                = np.zeros(ny_rans)
    y_RANS                  = np.arange(0,ny_rans*mergeCells,mergeCells)
    RANS_prob   = binning.double_binning(RANS_total[R["YO2"]],RANS_total[R["Z"]],RANS_total[R[condition_var2]],cstar2min,cstar2max,nbins1,nbins2,'count')
    RANS_cond[R["omega"],:,:,:] = binning.double_binning(RANS_total[R["omega"]],RANS_total[R["Z"]],RANS_total[R[condition_var2]],cstar2min,cstar2max,nbins1,nbins2)
    RANS_cond[R["tmp"],:,:,:]   = binning.double_binning(RANS_total[R["tmp"]],RANS_total[R["Z"]],RANS_total[R[condition_var2]],cstar2min,cstar2max,nbins1,nbins2)
    RANS_cond[R["rho"],:,:,:]   = binning.double_binning(RANS_total[R["rho"]],RANS_total[R["Z"]],RANS_total[R[condition_var2]],cstar2min,cstar2max,nbins1,nbins2)
    RANS_cond[R["YC7H16"],:,:,:]   = binning.double_binning(RANS_total[R["YC7H16"]],RANS_total[R["Z"]],RANS_total[R[condition_var2]],cstar2min,cstar2max,nbins1,nbins2)
    RANS_cond[R["YO2"],:,:,:]   = binning.double_binning(RANS_total[R["YO2"]],RANS_total[R["Z"]],RANS_total[R[condition_var2]],cstar2min,cstar2max,nbins1,nbins2)

    for i in range(ny_rans):
      RANS_omega_1D[i,:]  =   src.calc_omega(np.sum(RANS_cond[R["tmp"],i,:,:],axis=1)/np.sum(RANS_prob[i,:,:],axis=1),np.sum(RANS_cond[R["rho"],i,:,:],axis=1)/np.sum(RANS_prob[i,:,:],axis=1),np.sum(RANS_cond[R["YC7H16"],i,:,:],axis=1)/np.sum(RANS_prob[i,:,:],axis=1),np.sum(RANS_cond[R["YO2"],i,:,:],axis=1)/np.sum(RANS_prob[i,:,:],axis=1))
      RANS_omega[i,:,:]  =   src.calc_omega(RANS_cond[R["tmp"],i,:,:]/RANS_prob[i,:,:],RANS_cond[R["rho"],i,:,:]/RANS_prob[i,:,:],RANS_cond[R["YC7H16"],i,:,:]/RANS_prob[i,:,:],RANS_cond[R["YO2"],i,:,:]/RANS_prob[i,:,:])
      RANS_prob[i,:,:]   =   RANS_prob[i,:,:]/np.sum(RANS_prob[i,:,:])
      RANS_prob_1D[i,:]   =   np.sum(RANS_prob[i,:,:],axis=1)/np.sum(RANS_prob[i,:,:])

    RANS_omega_1D = np.nan_to_num(RANS_omega_1D)
    RANS_omega = np.nan_to_num(RANS_omega)
    RANS_prob_1D = np.nan_to_num(RANS_prob_1D)
    RANS_prob = np.nan_to_num(RANS_prob)
      
    RANS_CSE  = np.sum(RANS_prob_1D*RANS_omega_1D,axis=1)
    RANS_DCSE = np.sum(RANS_prob*RANS_omega,axis=(1,2))
    RANS_DNS  = np.mean(RANS_total[R["omega"],:,:,:],axis=(0,2))

    RANS_CSE = np.nan_to_num(RANS_CSE)
    RANS_DCSE = np.nan_to_num(RANS_DCSE)

    plt.plot(y_RANS,RANS_DCSE,label='y Averaged DCSE ')
    plt.plot(y_RANS,RANS_CSE,label='y Averaged CSE')
    plt.plot(y_RANS,RANS_DNS,label='y Averaged DNS')
    plt.xlabel('y index')
    plt.ylabel(r'reaction rate  mol (m$^3$ s)$^{-1}$ ')
    plt.legend()
    plt.tight_layout()
    plt.savefig("DCSE_avgd_sects.pdf")
    plt.show()
    
    pickle.dump(RANS_DNS, open( "RANS_DNS.p", "wb" ) )
    pickle.dump(RANS_CSE, open( "RANS_CSE.p", "wb" ) )
    pickle.dump(RANS_DCSE, open( "RANS_DCSE.p", "wb" ) )

###################################################
#
# Figure Generation (not completed)
#
###################################################

    #plt.plot([0,1E8], [0, 1E8],'k' ,lw=1, label='Perfect Correlation')
    #colours = np.arange(0,1,1.0/nbins1)#/np.full((nbins1),nbins1)
    #cm = plt.cm.get_cmap('gnuplot')
    #sc = plt.scatter(RANS_c[R["omega"],RANS_height,:],omega_avgd[y_center,:],s=10,c=colours, marker='o', alpha=0.8,label='Section at flame center',cmap=cm,vmin=0, vmax=1)
    #plt.colorbar(sc,label=r'Mixture fraction ($\zeta$)')
    #plt.xlabel(r'$\overline{\left. \dot{\omega}\right\vert \zeta }$, mol (m$^3$ s)$^{-1}$ - RANS')
    #plt.ylabel(r'$\overline{\left. \dot{\omega}\right\vert \zeta }$, mol (m$^3$ s)$^{-1}$ - DNS')
    #plt.xlim(0, 180)
    #plt.ylim(0, 180)
    #plt.legend()
    #plt.show()

##############



  #Visualization section
  #plt.plot(ycoord,avgProfile_total[:,R['rhoU']])
  #plt.legend()
  if scatter_plt_CSE_assumption:
    plt.plot([0,1E8], [0, 1E8],'k' ,lw=1, label='Perfect Corellation')
    colours = np.arange(0,1,1.0/nbins1)#/np.full((nbins1),nbins1)
    cm = plt.cm.get_cmap('gnuplot')
    omega_avgdvars,omega_avgd = binning.Merge_results_assumption(omega_sumtot,T_sumtot,rho_sumtot,Y1_sumtot,Y2_sumtot,Y3_sumtot,Y4_sumtot,c_numtot,nbins1,convert_omega_to_kg)
    sc = plt.scatter(omega_avgdvars[176,:],omega_avgd[176,:],s=10,c=colours, marker='o', alpha=0.8,label='Section at flame center',cmap=cm,vmin=0, vmax=1)
    plt.colorbar(sc,label=r'Mixture fraction ($\zeta$)')
    plt.xlabel(r'$\dot{\omega}\left( \overline{\left. \rho \right\vert \zeta },\overline{\left. T\right\vert \zeta },\overline{\left. Y_{i}\right\vert \zeta }\right)$, mol (m$^3$ s)$^{-1}$')
    plt.ylabel(r'$\overline{\left. \dot{\omega}\right\vert \zeta }$, mol (m$^3$ s)$^{-1}$ ')
    plt.xlim(0, 180)
    plt.ylim(0, 180)
    plt.legend()
    plt.show()

  if plot_avg_omega_by_y:
    omega_wrng = src.calc_omega(avgProfile_total[:,R["tmp"]],avgProfile_total[:,R["rho"]],avgProfile_total[:,R["YC7H16"]],avgProfile_total[:,R["YO2"]])
    omega1 = binning.Merge_Cond_results(T_sumtot,rho_sumtot,Y1_sumtot,Y2_sumtot,c_numtot,nbins1)
    plt.plot(avgProfile_total[:,R["omega"]],label = 'DNS')
    plt.plot(omega1, label='CSE and DNS-PDF')
    plt.plot(omega_wrng, label=r'$\dot{\omega}(\overline{T},\overline{\rho},\overline{Y_{\alpha}})$')
    plt.xlabel('y index')
    plt.ylabel(r'reaction rate  mol (m$^3$ s)$^{-1}$ ')
    plt.legend()
    plt.show()

  if compute_double_condition:
    mypdf3           = np.zeros((y_sections,nbins1))
    y_mid            = np.zeros(y_sections)
    mypdf4           = np.zeros((y_sections,nbins2))
    mult3            = np.zeros((y_sections,nbins1,nbins2))
    mult4            = np.zeros((y_sections,nbins1,nbins2))
    PDF_sections     = np.zeros([y_sections,nbins1,nbins2,nbAvgVar])
    oneD_PDF_sections= np.zeros([y_sections,nbins1,nbAvgVar])
    prob_DCSE_filt   = np.zeros([y_sections,nbins1,nbins2])
    omega_sections   = np.zeros([y_sections,nbins1,nbins2])
    oneD_omega_sections   = np.zeros([y_sections,nbins1])
    zero_check       = np.zeros([y_sections,nbins1,nbins2])
    zero_check2       = np.zeros([y_sections,nbins1])
    PDF_Var_sections = np.zeros([y_sections,nbins1,nbins2,nbAvgVar])
    Count_sections   = np.zeros([y_sections,nbins1,nbins2,nbAvgVar])
    PDF_Z_sections   = np.zeros([y_sections,nbins1,nbins2])
    oneD_PDF_Z_sections   = np.zeros([y_sections,nbins1])
    omega_truesect   = np.zeros(y_sections)
    ### fit PDFs ###

    for ii in range(nbAvgVar):
      PDF2[:,:,:,ii]         = binning.normalize(condition2_total[:,:,:,ii],mx*mz)

    DPDF = binning.normalize(condCount2_total[:,:,:],mx*mz)
    cindex= condCount2_total[:,:,:]>0

    for ii in range(nbAvgVar):
      condMean2_total[cindex,ii] = condition2_total[cindex,ii]/condCount2_total[cindex]
      condVar2_total[cindex,ii]  = condVar2_total[cindex,ii]/condCount2_total[cindex] - np.square(condMean2_total[cindex,ii])

    for ii in range(nbAvgVar):
      for jj in range(y_sections):
        aa        = y_sect_index[jj][0]
        bb        = y_sect_index[jj][-1]
        PDF_sections[jj,:,:,ii]   = np.sum(condition2_total[:,:,:,ii], axis=0)/np.sum(condCount2_total[:,:,:], axis=0)
        oneD_PDF_sections[jj,:,ii]  = np.sum(condition2_total[:,:,:,ii], axis=(0,2))/np.sum(condCount2_total[:,:,:], axis=(0,2))
        PDF_Z_sections[jj,:,:]    = np.sum(condCount2_total[aa:bb,:,:], axis=0)/np.sum(condCount2_total[aa:bb,:,:])
        oneD_PDF_Z_sections[jj,:] = np.sum(condCount2_total[aa:bb,:,:], axis=(0,2))/np.sum(condCount2_total[aa:bb,:,:])
        zero_check[jj,:,:]            = np.sum(condCount2_total[aa:bb,:,:], axis=0)
        zero_check2[jj,:]            = np.sum(condCount2_total[aa:bb,:,:], axis=(0,2))

    omega_sections = src.calc_omega(PDF_sections[:,:,:,R["tmp"]],PDF_sections[:,:,:,R["rho"]],PDF_sections[:,:,:,R["YC7H16"]],PDF_sections[:,:,:,R["YO2"]])
    oneD_omega_sections = src.calc_omega(oneD_PDF_sections[:,:,R["tmp"]],oneD_PDF_sections[:,:,R["rho"]],oneD_PDF_sections[:,:,R["YC7H16"]],oneD_PDF_sections[:,:,R["YO2"]])
    omega_sections[zero_check==0]=0
    oneD_omega_sections[zero_check2==0]=0

    for jj in range(0,y_sections):
      aa        = y_sect_index[jj][0]
      bb        = y_sect_index[jj][-1]
      y_mid[jj] = (bb-aa)/2 + aa
      mean_sec  = np.mean(avgProfile_total[aa:bb,R["Z"]])
      var_sec   = np.mean(avgVar_total[aa:bb,R["Z"]])
      omega_truesect[jj] = np.mean(avgProfile_total[aa:bb,R["omega"]])

      mypdf3[jj,:]       = distribution.compute_PDF(pdfType, mean_sec, var_sec,nbins1)
      mypdf3[jj,:]= mypdf3[jj,:]/np.sum(mypdf3[jj,:])
      mypdf3 = np.nan_to_num(mypdf3)

      mean_sec2  = (np.mean(avgProfile_total[aa:bb,R[condition_var2]])-cstar2min)/(cstar2max-cstar2min)
      var_sec2   = np.mean(avgVar_total[aa:bb,R[condition_var2]])/((cstar2max-cstar2min)*(cstar2max-cstar2min))
      mypdf4[jj,:]     = distribution.compute_PDF(pdfType2, mean_sec2, var_sec2,nbins2)
      mypdf4[jj,:]= mypdf4[jj,:]/np.sum(mypdf4[jj,:])
      mypdf4 = np.nan_to_num(mypdf4)

      ### plot PDF fits ###
      if plot_PDFS:
        print "Mean,Variance:", mean_sec,var_sec
        plt.plot(np.linspace(0,1,nbins1),np.sum(PDF_Z_sections[jj,:,:],axis=1)*nbins1,label='whole block')
        plt.plot(np.linspace(0,1,nbins1),np.sum(DPDF[aa,:,:],axis=1)*nbins1,label='bottom of block')
        plt.plot(np.linspace(0,1,nbins1),np.sum(DPDF[bb,:,:],axis=1)*nbins1,label='top of block')
        plt.plot(np.linspace(0,1,nbins1),mypdf3[jj,:],label='fitted function')
        #plt.plot(np.linspace(0,1,nbins1),PDF_sections[jj,:,R["Zcomp"]]*nbins1/float(mx*mz))
        plt.xlabel(r'1st conditioning variable ($\zeta$)')
        plt.legend()
        plt.show()

        plt.plot(np.linspace(cstar2min,cstar2max,nbins2),np.sum(PDF_Z_sections[jj,:,:],axis=0),label='whole block')
        plt.plot(np.linspace(cstar2min,cstar2max,nbins2),np.sum(PDF_Z_sections[jj,0:20,:],axis=0)/np.sum(PDF_Z_sections[jj,0:20,:]),label='z from 0 to 0.2')
        plt.plot(np.linspace(cstar2min,cstar2max,nbins2),np.sum(PDF_Z_sections[jj,20:40,:],axis=0)/np.sum(PDF_Z_sections[jj,20:40,:]),label='z from 0.2 to 0.4')
        plt.plot(np.linspace(cstar2min,cstar2max,nbins2),mypdf4[jj,:],label='fitted function')
        mypdf4[jj,:] = np.sum(PDF_Z_sections[jj,:,:],axis=0)
        plt.xlabel(r'2nd conditioning variable')
        plt.legend()
        plt.show()

    probs = np.zeros((my,nbins1))
    mypdf = np.zeros((my,nbins1))
    mypdf2 = np.zeros((my,nbins2))
    mypdf5 = np.zeros((my,nbins1,nbins2))
    xvals = np.zeros(nbins1*2)
    yvals = np.zeros(nbins1*2)
    yvals2 = np.zeros(nbins1*2)
    yvals3 = np.zeros(nbins1*2)
    yvals4 = np.zeros(nbins1*2)

    omega_DCSE,omega_CSE,omega_avg,probs,prob_DCSE,prob_both,omega_2d,omega_all,omega_all2,prob_all = binning.Merge_results_DCSE_assumption(condition2_total[:,:,:,R["omega"]],condition2_total[:,:,:,R["tmp"]],condition2_total[:,:,:,R["rho"]],condition2_total[:,:,:,R["YC7H16"]],condition2_total[:,:,:,R["YO2"]],condition2_total[:,:,:,R["YCO2"]],condition2_total[:,:,:,R["YH2O"]],condCount2_total,nbins1,nbins2,convert_omega_to_kg)

    pdfr2 = np.zeros((my,nbins1))

    #for y_cent in range(my/10):  
    for y_cent in range(my/400):
      mypdf[y_cent*10,:]    = distribution.compute_PDF(pdfType, avgProfile_total[y_cent*10,R["Z"]], avgVar_total[y_cent*10,R["Z"]],nbins1,avgProfile_total[y_cent*10,R["Zcomp"]])/float(nbins1)
      Omean  = avgProfile_total[y_cent*10,R["YO2"]]/Yoxy_top
      Ovar   = avgVar_total[y_cent*10,R["YO2"]]/Yoxy_top/Yoxy_top
      Fmean    = avgProfile_total[y_cent*10,R["YC7H16"]]/Yfuel_bot
      Fvar     = avgVar_total[y_cent*10,R["YC7H16"]]/Yfuel_bot/Yfuel_bot
      pdfr2[y_cent*10,:]  = distribution.convolution_PDF(pdfType, Fmean, Fvar, Omean,Ovar,Yoxy_top,Yfuel_bot,nbins1)
      mypdf = np.nan_to_num(mypdf)
      mypdf[y_cent*10,:]= mypdf[y_cent*10,:]/np.sum(mypdf[y_cent*10,:])
      pdfr2 = np.nan_to_num(pdfr2)
      pdfr2[y_cent*10,:]= pdfr2[y_cent*10,:]/np.sum(pdfr2[y_cent*10,:])
      pickle.dump(pdfr2[y_cent*10,:], open( "pdfr2.p", "wb" ) )
      pickle.dump(mypdf[y_cent*10,:], open( "beta.p", "wb" ) )
      pickle.dump(probs[y_cent*10,:], open( "pdf.p", "wb" ) )
      print x2_vort[y_cent*10],y_cent*10
      plt.hist(np.linspace(0,1,nbins1),nbins1, weights=probs[y_cent*10,:],label='DNS PDF',color='b',alpha=0.7)
      plt.plot(np.linspace(0,1,nbins1),pdfr2[y_cent*10,:],label='Beta-Convolution PDF')
      plt.plot(np.linspace(0,1,nbins1),mypdf[y_cent*10,:],label='Beta PDF')
      plt.xlabel(r'$\zeta$')
      plt.ylabel(r'$P(\zeta)$ ')
#plt.ylim(0,51000)
#plt.xlim(0,42000)
      plt.legend()
      plt.tight_layout()
      plt.savefig("PDF_conv.pdf")
      plt.show()

    ### plot 2D histogram of PDF ###
    Z = np.zeros((nbins1,nbins2))
    Xvals = np.zeros((nbins1,nbins2))
    Yvals = np.zeros((nbins1,nbins2))
    x3d = np.arange(0,1,1.0/nbins1)
    y3d = np.arange(cstar2min,cstar2max,(cstar2max-cstar2min)/nbins2)
    for i in range(nbins2):
      Xvals[:,i] = x3d
    for i in range(nbins1):
      Yvals[i,:] = y3d
    X,Y = np.meshgrid(y3d,x3d)
    Z = prob_all*nbins2*nbins1/(cstar2max-cstar2min)
    Xout = np.ravel(Xvals)
    Yout = np.ravel(Yvals)
    Zvals = np.ravel(Z)
    pickle.dump(Xout, open( "Xout.p", "wb" ) )
    pickle.dump(Yout, open( "Yout.p", "wb" ) )
    pickle.dump(Zvals, open( "Zout.p", "wb" ) )
    plt.hist2d(Xout, Yout, bins=[nbins1,nbins2], weights=Zvals)
    plt.colorbar(label=r'$P(\zeta,\rho)$')
    plt.xlabel(r'$\zeta$')
    plt.ylabel(r'$\rho$')
    plt.tight_layout()
    plt.savefig("2D_prob.pdf")
    plt.show()

    ### plot 1D histogram of PDF ###
    yvals[0::2] = np.sum(omega_all*prob_all,axis=1)
    yvals[1::2] = np.sum(omega_all*prob_all,axis=1)
    yvals2[0::2] = np.sum(omega_all2*prob_all,axis=1)
    yvals2[1::2] = np.sum(omega_all2*prob_all,axis=1)
    yvals3[0::2] = omega_avg[y_sect,:]
    yvals3[1::2] = omega_avg[y_sect,:]
    yvals4[0::2] = np.sum(omega_DCSE[y_sect,:,:]*prob_DCSE[y_sect,:,:],axis=1)
    yvals4[1::2] = np.sum(omega_DCSE[y_sect,:,:]*prob_DCSE[y_sect,:,:],axis=1)
    xvals[0::2] = np.arange(0,1,1.0/nbins1)
    xvals[1::2] = np.arange(1.0/nbins1,1+1.0/nbins1,1.0/nbins1)
    plt.plot(xvals,yvals,label=r'DNS - y=400-430')
    plt.plot(xvals,yvals3,label=r'DNS - y=415')
    plt.plot(xvals,yvals4,label=r'DCSE - y=415')
    plt.plot(xvals,yvals2,label=r'DCSE - y=400-430')
    plt.xlabel(r'$\zeta$')
    plt.ylabel(r'mol (m$^3$ s)$^{-1}$')
    plt.xlim(0, 1)
    plt.legend()
    plt.tight_layout()
    plt.savefig("CSE_assumpt.pdf")
    plt.show()


    ## fit PDFs to data ##

    for i in range(my):
      mypdf[i,:]    = distribution.compute_PDF(pdfType, avgProfile_total[i,R["Z"]], avgVar_total[i,R["Z"]],nbins1,avgProfile_total[i,R["Zcomp"]])/float(nbins1)
      Omean  = avgProfile_total[i,R["YO2"]]/Yoxy_top
      Ovar   = avgVar_total[i,R["YO2"]]/Yoxy_top/Yoxy_top
      Fmean    = avgProfile_total[i,R["YC7H16"]]/Yfuel_bot
      Fvar     = avgVar_total[i,R["YC7H16"]]/Yfuel_bot/Yfuel_bot
      pdfr2[i,:]  = distribution.convolution_PDF(pdfType, Fmean, Fvar, Omean,Ovar,Yoxy_top,Yfuel_bot,nbins1)
      mypdf = np.nan_to_num(mypdf)
      mypdf[i,:]= mypdf[i,:]/np.sum(mypdf[i,:])
      pdfr2 = np.nan_to_num(pdfr2)
      pdfr2[i,:]= pdfr2[i,:]/np.sum(pdfr2[i,:])
      if compute_PDF_2D:
        for j in range(nbins1):
          var_sec3  =    condVar_total[i,j,R[condition_var2]]/(cstar2max-cstar2min)/(cstar2max-cstar2min)
          mean_sec3  =     (condMean_total[i,j,R[condition_var2]]-cstar2min)/(cstar2max-cstar2min)
          print "mean,var", mean_sec3,var_sec3
          if mean_sec3 == 0 and var_sec3 ==0:
            mypdf5[i,j,:] =np.zeros(nbins2)
          else:
            mypdf5[i,j,:]     = distribution.compute_PDF(pdfType2, mean_sec3, var_sec3,nbins2,avgProfile_total[i,R["Zcomp"]])
          #print "pdf", mypdf5[i,j,:]
          mypdf5[omega_DCSE==0]=0
          mypdf5[i,j,:]= mypdf5[i,j,:]/np.sum(mypdf5[i,j,:])
          #print np.sum(mypdf5[i,j,:])
      print i

    mypdf5 = np.nan_to_num(mypdf5)
    mypdf = np.nan_to_num(mypdf)

    ## plot omega vs y ##
    CSE = np.sum(omega_CSE*probs,axis=1)
    PDFCSE2 = np.sum(omega_CSE*pdfr2,axis=1)
    DCSE = np.sum(omega_DCSE*prob_both,axis=(1,2))
    PDFCSE = np.sum(omega_CSE*mypdf,axis=1)
    omega_wrng = src.calc_omega(avgProfile_total[:,R["tmp"]],avgProfile_total[:,R["rho"]],avgProfile_total[:,R["YC7H16"]],avgProfile_total[:,R["YO2"]])

    PDFCSE = np.nan_to_num(PDFCSE)
    PDFCSE2 = np.nan_to_num(PDFCSE2)

    if compute_PDF_2D:
      PDFDCSE = np.sum(np.sum(omega_DCSE*mypdf5,axis=2)*mypdf,axis=1)
      PDFDCSE = np.nan_to_num(PDFDCSE)
      pickle.dump(avgProfile_total[:,R["omega"]], open( "DNS.p", "wb" ) )
      pickle.dump(PDFDCSE, open( "PDFDCSE.p", "wb" ) )
      plt.plot(PDFDCSE, label=r'DCSE and Beta-PDF for $\zeta$, inv-Gauss for $\rho$, correlated')

    pickle.dump(CSE, open( "CSE.p", "wb" ) )
    pickle.dump(DCSE, open( "DCSE.p", "wb" ) )
    pickle.dump(omega_wrng, open( "omega_wrng.p", "wb" ) )
    pickle.dump(PDFCSE, open( "PDFCSE.p", "wb" ) )
    pickle.dump(PDFCSE2, open( "PDFCSE2.p", "wb" ) )

    plt.plot(omega_wrng, label=r'$\dot{\omega}(\overline{T},\overline{\rho},\overline{Y_{\alpha}})$')
    plt.plot(avgProfile_total[:,R["omega"]],label = 'DNS')
    plt.plot(CSE, label='CSE and DNS-PDF')
    plt.plot(DCSE, label='DCSE and DNS-PDFs, correlated')
    plt.plot(PDFCSE, label='CSE and Beta-PDF')
    plt.plot(PDFCSE2, label='CSE and double beta pdf')
    plt.xlabel('y index')
    plt.ylabel(r'reaction rate  mol (m$^3$ s)$^{-1}$ ')
    plt.legend()
    plt.tight_layout()
    plt.savefig("DCSE_y_sects.pdf")
    plt.show()


    DCSE_filtered = np.sum(omega_sections*PDF_Z_sections,axis=(1,2)) 
    CSE_filtered  = np.sum(oneD_omega_sections*oneD_PDF_Z_sections,axis=1)
    DNS_filtered  = omega_truesect
    PDF_CSE_filtered  = np.sum(oneD_omega_sections*mypdf3,axis=1)

    pickle.dump(DNS_filtered, open( "DNS_filtered.p", "wb" ) )
    pickle.dump(CSE_filtered, open( "CSE_filtered.p", "wb" ) )
    pickle.dump(PDF_CSE_filtered, open( "PDF_CSE_filtered.p", "wb" ) )
    pickle.dump(DCSE_filtered, open( "DCSE_filtered.p", "wb" ) )
    pickle.dump(y_mid, open( "y_mid.p", "wb" ) )

    plt.plot(y_mid,DCSE_filtered,label='y Averaged DCSE ')
    plt.plot(y_mid,CSE_filtered,label='y Averaged CSE')
    plt.plot(y_mid,PDF_CSE_filtered,label='y Averaged CSE, Beta PDF')
    plt.plot(y_mid,DNS_filtered,label='y Averaged DNS')
    plt.xlabel('y index')
    plt.ylabel(r'reaction rate  mol (m$^3$ s)$^{-1}$ ')
    plt.legend()
    plt.tight_layout()
    plt.savefig("DCSE_avgd_sects.pdf")
    plt.show()

    #fig = plt.figure()
    #ax = fig.add_subplot(111, projection='3d')
    #ax.plot_surface(X,Y,Z, cmap=plt.cm.jet,cstride=1,rstride=1)
    #plt.show()

  #omega_avgtot = omega_avgtot/binvaltot
  #omega_wrng_avgtot = omega_wrng_avgtot/binvaltot
  #omega1 = binning.Merge_Cond_results(T_sumtot,rho_sumtot,Y1_sumtot,Y2_sumtot,c_numtot,nbins1)
  #
  #plt.plot(omega1,label = 'CSE')
  #normX,omega_fig6=np.loadtxt("Bellan2017_fig6a.csv",unpack=True)
  #plt.plot(np.linspace(1,0,my),avgProfile_total[:,R["omega"]],label = 'DNS')
  #plt.scatter(normX,omega_fig6,label = 'paper')
  #plt.legend()
  #plt.show()
  #plt.plot(maxProfile_total[:,R["omega"]],label = 'CSE')
  #plt.plot(omega_avgtot, label='DNS')
  #plt.plot(omega_wrng_avgtot,label = 'omega from mean props')
  #plt.scatter(RANS_total[R["omega"]],RANS_total[R["filtered_omega"]])
  #plt.plot([0,5E6],[0,5E6])
  #plt.xlim(0,5E4)

#for var in range(5,9):
#  plt.plot(avgProfile[:,var]/float(npneed/npy),label=str(var))
 # plt.xlabel('y section')
 # plt.ylabel('reaction rate mol (m^3 s)^-1')
